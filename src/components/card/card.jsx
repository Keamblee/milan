import React from 'react'
import "./card.css"

const Card = (props) => {
    return(
    <div className="size">
        <div style={{backgroundImage: `url(${props.date.image})`}} className="background">
            <div className="hover"></div>
        </div>
        <div className="text"></div>
    </div>
    )
}
export default Card