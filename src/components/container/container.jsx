import React from 'react'
import Card from '../card/card'
import "./container.css"



const Container = (props) => {
    return(
    <div className="grid">
        {props.date.map((i,index)=>{
           return <Card date = {i}></Card>
        })}
    </div>
    )
}
export default Container