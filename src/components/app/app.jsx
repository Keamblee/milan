import React from "react"
import "./app.css"
import background from "./img/background.png"
import Container from "../container/container"



const Date = [
    {name : "космос",
    image: "https://www.techcult.ru/content/2020/9048/glubokij-kosmos_sm.jpg",
    description: `Косми́ческое простра́нство, ко́смос (др.-греч. κόσμος «мир», «Вселенная») — относительно пустые участки Вселенной, которые лежат вне границ атмосфер небесных тел. Космос не является абсолютно пустым пространством: в нём есть, хотя и с очень низкой плотностью, межзвёздное вещество (преимущественно молекулы водорода), кислород в малых количествах (остаток после взрыва звезды), космические лучи и электромагнитное излучение, а также гипотетическая тёмная материя.`},
    {name : "Кот",
    image: "https://pics.botanichka.ru/wp-content/uploads/2019/11/chto-nam-hochet-skazat-01.jpg",
    description: `домашнее животное, одно из наиболее популярных «животных-компаньонов». С точки зрения научной систематики, домашняя кошка — млекопитающее семейства кошачьих отряда хищных. Ранее домашнюю кошку нередко рассматривали как отдельный биологический вид.`},
    {name : "Солнце",
    image: "https://user30887.clients-cdnnow.ru/uploads/5f42be1ca211d166333997.jpg",
    description: `Со́лнце — одна из звёзд нашей Галактики и единственная звезда Солнечной системы. Вокруг Солнца обращаются другие объекты этой системы: планеты и их спутники, карликовые планеты и их спутники, астероиды, метеороиды, кометы и космическая пыль. По спектральной классификации Солнце`},
  ]
const App = () => {
    return(
        <main >
            <div className="main" style={{backgroundImage:`url(${background})`}}>
                <Container date={Date}></Container>
            </div>
        </main>
    )
}
console.log(background)
export default App